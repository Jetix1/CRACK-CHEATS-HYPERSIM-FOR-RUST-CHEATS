# CRACK-CHEATS-HYPERSIM-FOR-RUST FREE CHEATS

# SYSTEM REQUIREMENTS:

- Intel & Amd Processor;
- Windows 10/8.1/8

# How to install?

- Download the archive 

- Unpack the archive to any location ( Password from the archive is GudCrack )

- Run the file

- Launch the game

- In-game INSERT button

# Functions

#AIMBOT

- Enable toggle
- pSilent
- Smoothing
- FOV limit
- Prediction
- Magic bullets
- Magic bullet auto-shoot
- Magic bullet rapid-fire
- Magic bullet for helicopter
- Customizable amount of magic bullet scanning points (performance)
- Visible check
- Target locking (avoid snapping to other target)
- Target list (Players, Friends, NPCs, Heli)
- Hitbox
- No spread
- No sway
- Faster bullets
- Horizontal & Vertical recoil sliders
- Disable attack restrictions (shoot while jumping)
- Instant Eoka
- Instant compound bow
- Bullet thickness
- Ignore damage for teammates
- Hit override
- Hit override for helicopter

# ESP

- Player ESP
- Type: Players, Sleepers, NPC
- Box
- Skeleton
- Name
- Distance
- Health
- Flags (safe zone, wounded)
- Active item
- Draggable hotbar
- Visible chams
- Player-highlight/target colors
- Ore ESP
- Stone, Sulfur, Metal ores
- Collectible ESP
- Wood, Stone, Sulfur, Metal, Mushroom, Pumpkin, Corn, Potato, Hemp
- Radtown ESP
- Barrels, Crates, Toolboxes, Foodboxes, Other (vehicle parts..)
- Animal ESP
- Bear, Boar, Wolf, Stag
- Stash ESP
- Hidden & Open stashes, Filter open stashes
- Vehicle ESP
- Minicopter, scrap helicopter
- Boats, RHIBs
- Horses, baloons
- Patrol helicopter, Bradley
- Trap ESP
- Filter disabled traps
- Auto turrets (friendly + enemy color)
- Hide authorized auto turrets
- SAM sites
- Shotgun traps
- Flame turrets
- Land mines
- Bear traps
- Item ESP
- Items, player backpack, corpse(+ player name)
- Sleeping bag ESP
- Tool cupboard ESP
- Show only decaying
- Authorized player list & FOV limit
- Raid ESP
- C4, Satchel, Rocket, Inc. Rocket, Exp. Ammo, Exp. grenade, raid start, last explosion, timeout
- Watermark & prediction point

# MISC

- Movement
- Spiderman
- Infinite jump
- Jesus
- Ignore tree collision
- Omni-sprint (sprint in all directions)
- Silent walk
- Flyhack
- No collide
- Anti-flyhack kick
- Phase
- Giraffe (view offset/fakeduck)
- Player
- Admin flag (allows you to use client-side admin commands)
- Equip items while mounted
- No fall damage
- Instant revive
- Instant med
- Tick interval (allows you to modify packet amount sent in second)
- Whitelist player

# Other

- Instant loot
- Weapon spam
- Silent melee
- Silent farm (trees & ores)
- Hemp seed spam (saved position, nearest player, camera)
- Auto collect
- Bright cave
- Bright night (brightness customizable)
- Interactive debug
- Automatic floor-stack
- Rotation offset for building
- Zoom hack
- FOV changer
- Hide attire overlays

# PLAYER LIST

- Full player list from all players retrieved on server
- Gathers information from met players, sleeping bags, tool cupboards
- Shows name, steamid, adding into friendlist, highlighting on ESP, priority for AIM
- Shows last known position for Player, TC, BAG

![1670253951456](https://user-images.githubusercontent.com/120755785/208197712-608c81dd-4678-4218-b8c2-4475903a0aa9.png)
